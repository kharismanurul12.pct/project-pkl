<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\PageController;
use App\Http\Controllers\EditorController;
use App\Http\Controllers\JurnalController;
use App\Http\Controllers\KonfirmasiController;
use App\Http\Controllers\LoginController;
use App\Http\Controllers\PenilaianController;
use App\Http\Controllers\ReviewerController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::get('/', [PageController::class, 'home']);
Route::get('/editor', [PageController::class, 'editor']);
Route::get('/jurnal', [PageController::class, 'jurnal']);
Route::get('/konfirmasi', [PageController::class, 'konfirmasi']);
Route::get('/penilaian', [PageController::class, 'penilaian']);
Route::get('/reviewer', [PageController::class, 'reviewer']);

Route::group(['prefix' => '/editor'], function (){
    Route::get('/', [EditorController::class,'index']);
    Route::get('/add', [EditorController::class,'create']);
    Route::post('/create', [EditorController::class,'save']);
    Route::get('/delete/{id}', [EditorController::class,'delete']);
    Route::get('/edit/{id}', [EditorController::class,'edit']);
    Route::post('/update', [EditorController::class,'update']);
});

Route::group(['prefix' => '/food'], function (){
    Route::get('/', [FoodController::class,'index']);
    Route::get('/add', [FoodController::class,'create']);
    Route::post('/create', [FoodController::class,'save']);
    Route::get('/delete/{id}', [FoodController::class,'delete']);
    Route::get('/edit/{id}', [FoodController::class,'edit']);
    Route::post('/update', [FoodController::class,'update']);
});

Route::group(['prefix' => '/food'], function (){
    Route::get('/', [FoodController::class,'index']);
    Route::get('/add', [FoodController::class,'create']);
    Route::post('/create', [FoodController::class,'save']);
    Route::get('/delete/{id}', [FoodController::class,'delete']);
    Route::get('/edit/{id}', [FoodController::class,'edit']);
    Route::post('/update', [FoodController::class,'update']);
});

Route::group(['prefix' => '/food'], function (){
    Route::get('/', [FoodController::class,'index']);
    Route::get('/add', [FoodController::class,'create']);
    Route::post('/create', [FoodController::class,'save']);
    Route::get('/delete/{id}', [FoodController::class,'delete']);
    Route::get('/edit/{id}', [FoodController::class,'edit']);
    Route::post('/update', [FoodController::class,'update']);
});

Route::group(['prefix' => '/food'], function (){
    Route::get('/', [FoodController::class,'index']);
    Route::get('/add', [FoodController::class,'create']);
    Route::post('/create', [FoodController::class,'save']);
    Route::get('/delete/{id}', [FoodController::class,'delete']);
    Route::get('/edit/{id}', [FoodController::class,'edit']);
    Route::post('/update', [FoodController::class,'update']);
});


Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
