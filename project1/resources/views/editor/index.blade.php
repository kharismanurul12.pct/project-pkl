@extends('layouts.app')
@section('title', 'Halaman Editor')
@section('main')
@guest
<div class="container">
    <div class="row mt-3 mb-3">
        @foreach($data as $editor)
            <div class="col-3 mb-3">
            <a class="text-decoration-none text-black" href="{{ url('/editor/detail/' .$editor->id) }}">
                <div class="card">
                    <div class="card-header">
                        <h3>{{ $editor->title }}</h3>
                    </div>
                    <div class="card-body">
                        @if(strlen($editor->description) > 100)
                            {{ substr_replace($editor->description,"...", 100) }}
                        @else
                            {{ $editor->description }}
                        @endif
                    </div>
                </div>
            </a>
            </div>
    @endforeach
    </div>
</div>

@else
<div class="container">
    <div class="row mt-3 mb-3">
        <a class="btn btn-primary mb-3" href="{{ url('/editor/add') }}">Tambah Data</a>
        @foreach($data as $editor)
            <div class="col-3 mb-3">
            <a class="text-decoration-none text-black" href="{{ url('/editor/detail/' .$editor->id) }}">
                <div class="card">
                    <div class="card-header">
                        <h3>{{ $editor->title }}</h3>
                    </div>
                    <div class="card-body">
                        @if(strlen($editor->description) > 100)
                            {{ substr_replace($editor->description,"...", 100) }}
                        @else
                            {{ $editor->description }}
                        @endif
                    </div>
                    <div class="card-footer">
                    <a href="{{ url('/editor/edit/'.$editor->id) }}" class="btn btn-warning btn-sm">Edit</a>
                    <a href="{{ url('/editor/delete/'.$editor->id) }}" class="btn btn-danger btn-sm">Hapus</a>
                    </div>
                </div>
            </a>
            </div>
    @endforeach
    </div>
</div>
@endguest
@endsection