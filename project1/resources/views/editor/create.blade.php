@extends('layouts.app')
@section('title', 'Halaman Editor')
@section('main')
<div class="container">
    <div class="row mt-3 mb-3">
        <form action="{{ url('/editor/create') }}" method="post">
            @csrf
            <div class="mb-3">
                <label>Nama</label>
                <input type="text" class="form-control" name="name">
            </div> 
            <div class="mb-3">
                <label>NIDN</label>
                <input type="text" class="form-control" name="nidn">
            <div class="mb-3">
                <label>Keilmuan</label>
                <textarea name="bidang" class="form-control"></textarea>
            </div> 
            <div class="mb-3">
                <button class="btn btn-primary">Submit</button>
            </div>  
        </form>
    </div>
</div>
@endsection