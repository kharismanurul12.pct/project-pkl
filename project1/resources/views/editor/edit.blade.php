@extends('layouts.app')
@section('title', 'Halaman Editor')
@section('main')
<div class="container">
    <div class="row mt-3 mb-3">
        <form action="{{ url('/editor/update') }}" method="post">
            @csrf
            <input type="hidden" name="id" value="{{ $editor->id }}" >
            <div class="mb-3">
                <label>Nama</label>
                <input type="text" class="form-control" name="name" value="{{ $editor->name }}">
            </div> 
            <div class="mb-3">
                <label>NIDN</label>
                <input type="text" class="form-control" name="nidn" value="{{ $editor->price }}">
            <div class="mb-3">
                <label>Keilmuan</label>
                <textarea name="bidang" class="form-control">{{ $editor->bidang }}</textarea>
            </div> 
            <div class="mb-3">
                <button class="btn btn-primary">Submit</button>
            </div>  
        </form>
    </div>
</div>
@endsection