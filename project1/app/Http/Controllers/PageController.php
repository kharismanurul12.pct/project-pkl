<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PageController extends Controller
{   
    function home(){
        return view('welcome');
    }
    function editor(){
        return view('editor');
    }
    function jurnal(){
        return view('jurnal');
    }
    function konfirmasi(){
        return view('konfirmasi');
    }
    function penilaian(){
        return view('penilaian');
    }
    function reviewer(){
        return view('reviewer');
    }
}
