<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class KonfirmasiController extends Controller
{
    public function index(){
        $data = Konfirmasi::orderBy('id','desc')->get();
        return view('konfirmasi.index', ['data' => $data]);
    }

    public function create(){
        return view('konfirmasi.create');
    }

    public function save(Request $request){
        $konfirmasi = New Konfirmasi();
        $konfirmasi->konfirmasi = $request->konfirmasi;
        $konfirmasi->save(); 
        return redirect('/konfirmasi');
    }
}
